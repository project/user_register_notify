<?php

namespace Drupal\user_register_notify\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\token\TreeBuilder;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The module's settings form.
 */
class UserRegisterNotifyAdminSettingsForm extends ConfigFormBase {

  /**
   * The mail validator instance.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $mailValidator;

  /**
   * The token tree builder instance.
   *
   * @var \Drupal\token\TreeBuilder
   */
  protected $tokenTreeBuilder;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $mail_validator
   *   The mail validator instance.
   * @param \Drupal\token\TreeBuilder $token_tree_builder
   *   The token tree builder instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EmailValidatorInterface $mail_validator, TreeBuilder $token_tree_builder) {
    parent::__construct($config_factory);
    $this->mailValidator = $mail_validator;
    $this->tokenTreeBuilder = $token_tree_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('email.validator'),
      $container->get('token.tree_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_register_notify_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['user_register_notify.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $site_config = $this->config('system.site');
    $register_config = $this->config('user_register_notify.settings');

    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Registration notification type'),
      '#description' => $this->t('Where to send the registration notification to.'),
      '#default_value' => $register_config->get('type'),
      '#options' => [
        'disabled' => $this->t('Disabled (do not send any notifications)'),
        'role' => $this->t('Send Email to selected role(s)'),
        'custom' => $this->t('Send Email to custom Email address(es)'),
        'both' => $this->t('Send Email to both custom Email address(es) and specific role(s)'),
      ],
    ];
    $form['mail_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email address(es) to notify'),
      '#description' => $this->t('Comma separated list of Email addresses to be used as the "to" address for all user notifications listed below. Leave empty to use the default system Email address <em>(%site-email).</em>', [
        '%site-email' => $site_config->get('mail') ?? $this->t('- None -'),
      ]),
      '#default_value' => $register_config->get('mail_to'),
      '#states' => [
        'visible' => [
          // This uses "or" implicitly through wrapping each input in an array:
          [':input[name="type"]' => ['value' => 'custom']],
          [':input[name="type"]' => ['value' => 'both']],
        ],
      ],
    ];
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Notify these roles by Email'),
      '#description' => $this->t('All users with these checked roles will receive a notification Email when selected.'),
      '#default_value' => $register_config->get('roles') ?? [],
      '#options' => $this->getRoles(),
      '#states' => [
        'visible' => [
          // This uses "or" implicitly through wrapping each input in an array:
          [':input[name="type"]' => ['value' => 'role']],
          [':input[name="type"]' => ['value' => 'both']],
        ],
      ],
    ];
    $form['events'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Notify on these events'),
      '#description' => $this->t('When to send an Email to the specified roles / custom Email addresses.'),
      '#default_value' => $register_config->get('events') ?? [],
      '#required' => FALSE,
      '#options' => [
        'create' => $this->t('User created'),
        'update' => $this->t('User updated / edited'),
        'delete' => $this->t('User deleted'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['!value' => 'disabled'],
        ],
      ],
    ];

    // User updated:
    $form['user_register_notify_created'] = [
      '#type' => 'details',
      '#title' => $this->t('User creation Email notification'),
      '#description' => $this->t('Edit the notification Email message sent, if a new user user was created (Based on the following roles: and / or custom email adresses provided above).'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          // This uses "and" implicitly through not wrapping each input in an
          // array:
          ':input[name="events[create]"]' => ['checked' => TRUE],
          ':input[name="type"]' => ['!value' => 'disabled'],
        ],
      ],
    ];
    $form['user_register_notify_created']['created_roles_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('If the created user'),
      '#default_value' => $register_config->get('created_roles_mode'),
      '#options' => [
        'include' => $this->t('has any of the following roles:'),
        'exclude' => $this->t('has none of the following roles:'),
      ],
    ];
    $form['user_register_notify_created']['created_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('User roles'),
      '#title_display' => 'invisible',
      '#description' => $this->t('If no roles are selected, the notification is sent for any created user, despite his role(s).'),
      '#default_value' => $register_config->get('created_roles'),
      '#options' => $this->getRoles(),
    ];
    $form['user_register_notify_created']['created_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('Subject of the user created notification Email.'),
      '#default_value' => $register_config->get('created_subject'),
      '#required' => TRUE,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
    ];
    $form['user_register_notify_created']['created_body'] = [
      '#type' => 'textarea',
      '#default_value' => $register_config->get('created_body'),
      '#description' => $this->t('Customize the body of the user created notification Email.'),
      '#required' => TRUE,
      '#rows' => 10,
      '#title' => $this->t('Body'),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
    ];
    $form['user_register_notify_created']['created_token_tree'] = [
      '#theme' => 'token_tree_link',
      '#global_types' => TRUE,
      '#token_types' => ['user'],
      '#dialog' => TRUE,
      '#click_insert' => TRUE,
      '#show_restricted' => FALSE,
    ];

    // User updated:
    $form['user_register_notify_updated'] = [
      '#type' => 'details',
      '#title' => $this->t('User update / edit Email Notification'),
      '#description' => $this->t('Edit the notification Email message sent, if a user user was updated (Based on the following roles: and / or custom email adresses provided above).'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          // This uses "and" implicitly through not wrapping each input in an
          // array:
          ':input[name="events[update]"]' => ['checked' => TRUE],
          ':input[name="type"]' => ['!value' => 'disabled'],
        ],
      ],
    ];
    $form['user_register_notify_updated']['updated_roles_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('If the updated user'),
      '#default_value' => $register_config->get('updated_roles_mode'),
      '#options' => [
        'include' => $this->t('has any of the following roles:'),
        'exclude' => $this->t('has none of the following roles:'),
      ],
    ];
    $form['user_register_notify_updated']['updated_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('User roles'),
      '#title_display' => 'invisible',
      '#description' => $this->t('If no roles are selected, the notification is sent for any updated user, despite his role(s).'),
      '#default_value' => $register_config->get('updated_roles'),
      '#options' => $this->getRoles(),
    ];
    $form['user_register_notify_updated']['updated_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('Subject of user updated messages.'),
      '#default_value' => $register_config->get('updated_subject'),
      '#required' => TRUE,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
    ];
    $form['user_register_notify_updated']['updated_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Customize the body of the user updated notification Email.'),
      '#default_value' => $register_config->get('updated_body'),
      '#required' => TRUE,
      '#rows' => 10,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
    ];
    $form['user_register_notify_updated']['updated_token_tree'] = [
      '#theme' => 'token_tree_link',
      '#global_types' => TRUE,
      '#token_types' => ['user'],
      '#dialog' => TRUE,
      '#click_insert' => TRUE,
      '#show_restricted' => FALSE,
    ];

    // User deleted:
    $form['user_register_notify_deleted'] = [
      '#type' => 'details',
      '#title' => $this->t('User deleted Email Notification'),
      '#description' => $this->t('Edit the notification Email message sent, if a user  was deleted (Based on the following roles: and / or custom email adresses provided above).'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          // This uses "and" implicitly through not wrapping each input in an
          // array:
          ':input[name="events[delete]"]' => ['checked' => TRUE],
          ':input[name="type"]' => ['!value' => 'disabled'],
        ],
      ],
    ];
    $form['user_register_notify_deleted']['deleted_roles_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('If the deleted user'),
      '#default_value' => $register_config->get('deleted_roles_mode'),
      '#options' => [
        'include' => $this->t('has any of the following roles:'),
        'exclude' => $this->t('has none of the following roles:'),
      ],
    ];
    $form['user_register_notify_deleted']['deleted_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('User roles'),
      '#title_display' => 'invisible',
      '#description' => $this->t('If no roles are selected, the notification is sent for any deleted user, despite his role(s).'),
      '#default_value' => $register_config->get('deleted_roles'),
      '#options' => $this->getRoles(),
    ];
    $form['user_register_notify_deleted']['deleted_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('Subject of user deleted messages.'),
      '#default_value' => $register_config->get('deleted_subject'),
      '#required' => TRUE,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
    ];
    $form['user_register_notify_deleted']['deleted_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Customize the body of the user deleted notification Email.'),
      '#default_value' => $register_config->get('deleted_body'),
      '#required' => TRUE,
      '#rows' => 10,
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
    ];
    $form['user_register_notify_deleted']['deleted_token_tree'] = [
      '#theme' => 'token_tree_link',
      '#global_types' => TRUE,
      '#token_types' => ['user'],
      '#dialog' => TRUE,
      '#click_insert' => TRUE,
      '#show_restricted' => FALSE,
    ];

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced Settings'),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['!value' => 'disabled'],
        ],
      ],
    ];

    $form['advanced_settings']['enable_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging'),
      '#description' => $this->t('Create a Drupal log entry whenever a user notification is sent. Useful for debugging or as additional log.'),
    ];

    // User Email header overrides:
    $form['advanced_settings']['risky_mail_header_modifications'] = [
      '#type' => 'details',
      '#title' => $this->t('User Email header overrides'),
      '#description' => $this->t('The following settings apply to *all* user emails selected below. You should probably not set these unless you have specific requirements. In particular, modifying the "from" address could result in Email being blocked as spam, and the response could impact on your server.'),
    ];
    $form['advanced_settings']['risky_mail_header_modifications']['mail_message_ids_header_overwrite_header'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('System mail messages to override'),
      '#description' => $this->t('The system mail messages to overwrite "From" and "Reply-to" on.'),
      '#default_value' => $register_config->get('mail_message_ids_header_overwrite_header') ?? [],
      '#options' => [
        // Our module mail ids:
        'user_register_notify_user_register_notify_create' => $this->t('User register notify: User created %key', ['%key' => 'user_register_notify_user_register_notify_create']),
        'user_register_notify_user_register_notify_update' => $this->t('User register notify: User updated %key', ['%key' => 'user_register_notify_user_register_notify_update']),
        'user_register_notify_user_register_notify_delete' => $this->t('User register notify: User deleted %key', ['%key' => 'user_register_notify_user_register_notify_delete']),
        // Core - User module.
        'user_register_admin_created' => $this->t('Welcome (new user created by administrator) %key', ['%key' => 'user_register_admin_created']),
        'user_register_no_approval_required' => $this->t('Welcome (no approval required) %key', ['%key' => 'user_register_no_approval_required']),
        // If a user registered requiring admin approval, notify the admin, too.
        'user_register_pending_approval_admin' => $this->t('Admin (user awaiting approval) %key', ['%key' => 'user_register_pending_approval_admin']),
        'user_register_pending_approval' => $this->t('Welcome (awaiting approval) %key', ['%key' => 'user_register_pending_approval']),
        'user_status_activated' => $this->t('User activation %key', ['%key' => 'user_status_activated']),
        'user_status_blocked' => $this->t('User blocked %key', ['%key' => 'user_status_blocked']),
        'user_cancel_confirm' => $this->t('User cancellation confirmation %key', ['%key' => 'user_cancel_confirm']),
        'user_status_canceled' => $this->t('User canceled %key', ['%key' => 'user_status_canceled']),
        'user_password_reset' => $this->t('Password recovery %key', ['%key' => 'user_password_reset']),
      ],
    ];
    $form['advanced_settings']['risky_mail_header_modifications']['mail_from'] = [
      '#type' => 'email',
      '#title' => $this->t('Override "From" mail header'),
      '#description' => $this->t('The Email address to be used as the "from" address for all user notifications listed below. Leave empty to use the default system Email address <em>(%site-email).</em>', [
        '%site-email' => $site_config->get('mail'),
      ]),
      '#default_value' => $register_config->get('mail_from'),
    ];
    $form['advanced_settings']['risky_mail_header_modifications']['mail_reply_to'] = [
      '#type' => 'email',
      '#title' => $this->t('Override "Reply-to" mail header'),
      '#description' => $this->t('The Email address to be used as the "reply-to" address for all user notifications listed below. Leave empty for not using "reply-to" Email header. Configure this field only, if your "reply-to" address is not your "from" address.'),
      '#default_value' => $register_config->get('mail_reply_to'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Cleanup the Email string.
    $user_register_notify_mail_to = explode(',', trim($form_state->getValue('mail_to')));
    $form_state->setValue('mail_to', implode(',', $user_register_notify_mail_to));

    if (in_array($form_state->getValue('type'), ['custom', 'both'])) {
      foreach ($user_register_notify_mail_to as $email) {
        if (!$this->mailValidator->isValid($email)) {
          $form_state->setErrorByName('mail_to', $this->t('The Email address %mail is not valid.', [
            '%mail' => $email,
          ]));
        }
      }
    }
  }

  /**
   * Get all roles, except the anonymous user.
   *
   * @return array
   *   The roles.
   */
  protected function getRoles() {
    return array_map(function (RoleInterface $role) {
      if ($role->id() !== 'anonymous') {
        return $role->label();
      }
    }, Role::loadMultiple());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('user_register_notify.settings')
      ->set('type', $form_state->getValue('type'))
      ->set('mail_to', $form_state->getValue('mail_to'))
      ->set('roles', $form_state->getValue('roles'))
      ->set('created_subject', $form_state->getValue('created_subject'))
      ->set('created_body', $form_state->getValue('created_body'))
      ->set('updated_subject', $form_state->getValue('updated_subject'))
      ->set('updated_body', $form_state->getValue('updated_body'))
      ->set('deleted_subject', $form_state->getValue('deleted_subject'))
      ->set('deleted_body', $form_state->getValue('deleted_body'))
      ->set('mail_from', $form_state->getValue('mail_from'))
      ->set('mail_reply_to', $form_state->getValue('mail_reply_to'))
      ->set('events', $form_state->getValue('events'))
      ->set('mail_message_ids_header_overwrite_header', $form_state->getValue('mail_message_ids_header_overwrite_header'))
      ->set('created_roles_mode', $form_state->getValue('created_roles_mode'))
      ->set('created_roles', $form_state->getValue('created_roles'))
      ->set('updated_roles_mode', $form_state->getValue('updated_roles_mode'))
      ->set('updated_roles', $form_state->getValue('updated_roles'))
      ->set('deleted_roles_mode', $form_state->getValue('deleted_roles_mode'))
      ->set('deleted_roles', $form_state->getValue('deleted_roles'))
      ->set('enable_logging', $form_state->getValue('enable_logging'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
