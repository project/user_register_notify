<?php

namespace Drupal\Tests\user_register_notify\Functional;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group user_register_notify
 */
class UserRegisterNotifyFunctionalTest extends BrowserTestBase {
  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'test_page_test',
    'user_register_notify',
    'user',
    'token',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * The module settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $moduleSettings;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->moduleSettings = $this->config('user_register_notify.settings');
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if installing the module, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    // Go to uninstallation page an uninstall user_register_notify:
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-user-register-notify');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm uninstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
    // Retest the frontpage:
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoCustomMailAddressCreated() {
    $this->moduleSettings
      ->set('type', 'custom')
      ->set('mail_to', 'admin@test.com')
      ->set('created_subject', 'Test Subject')
      ->set('created_body', 'Test Body')
      ->set('events', ['create'])
      ->save();
    $this->createUser([], 'testUser', FALSE);
    $this->drupalGet('/admin/people/create');
    $this->assertMail('id', 'user_register_notify_user_register_notify_create');
    $this->assertMail('to', 'admin@test.com');
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoMultipleCustomMailAddressCreated() {
    $this->moduleSettings
      ->set('type', 'custom')
      ->set('mail_to', 'admin@test.com admin2@test.com')
      ->set('created_subject', 'Test Subject')
      ->set('created_body', 'Test Body')
      ->set('events', ['create'])
      ->save();
    $this->createUser([], 'testUser', FALSE);
    $this->assertMail('id', 'user_register_notify_user_register_notify_create');
    $this->assertMail('to', 'admin@test.com admin2@test.com');
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoRoleMailAddressCreated() {
    $this->createRole([], 'mail_recipient', 'mail_recipient');
    $this->drupalGet('/admin/people/create');

    $this->submitForm([
      'edit-mail' => 'mail@recipient.com',
      'edit-name' => 'User',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');

    $this->moduleSettings
      ->set('type', 'role')
      ->set('roles', ['mail_recipient'])
      ->set('created_subject', 'Test Subject')
      ->set('created_body', 'Test Body')
      ->set('events', ['create'])
      ->save();

    $user = $this->createUser();
    // Change username, to invoke the update event:
    $user->setUsername('test123');

    $this->assertMail('id', 'user_register_notify_user_register_notify_create');
    $this->assertMail('to', 'mail@recipient.com');
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoRoleMailAddressUpdated() {
    $this->createRole([], 'mail_recipient', 'mail_recipient');
    $this->drupalGet('/admin/people/create');

    $this->submitForm([
      'edit-mail' => 'mail@recipient.com',
      'edit-name' => 'User',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');

    $this->moduleSettings
      ->set('type', 'role')
      ->set('events', ['update'])
      ->set('roles', ['mail_recipient'])
      ->set('updated_subject', 'Test Subject')
      ->set('updated_body', 'Test Body')
      ->save();

    $user = $this->createUser();
    $this->drupalGet('/user/' . $user->id() . '/edit');
    $this->submitForm([
      'edit-mail' => 'changedMail@test.com',
      'edit-pass-pass1' => $user->getPassword(),
      'edit-pass-pass2' => $user->getPassword(),
    ], 'Save');

    $this->assertMail('id', 'user_register_notify_user_register_notify_update');
    $this->assertMail('to', 'mail@recipient.com');
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoRoleMailAddressDeleted() {
    $this->createRole([], 'mail_recipient', 'mail_recipient');
    $this->drupalGet('/admin/people/create');

    $this->submitForm([
      'edit-mail' => 'mail@recipient.com',
      'edit-name' => 'User',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');

    $this->moduleSettings
      ->set('type', 'role')
      ->set('events', ['delete'])
      ->set('roles', ['mail_recipient'])
      ->set('deleted_subject', 'Test Subject')
      ->set('deleted_body', 'Test Body')
      ->save();

    $user = $this->createUser();
    $user->delete();
    $this->assertMail('id', 'user_register_notify_user_register_notify_delete');
    $this->assertMail('to', 'mail@recipient.com');
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoRoleMultipleRecipientsMailAddressCreated() {
    $this->createRole([], 'mail_recipient', 'mail_recipient');
    // Add an mail recipient account:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'mail@recipient.com',
      'edit-name' => 'User',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');
    // Add another mail recipient account:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'anotherMail@recipient.com',
      'edit-name' => 'User2',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');

    $this->moduleSettings
      ->set('type', 'role')
      ->set('roles', ['mail_recipient'])
      ->set('created_subject', 'Test Subject')
      ->set('created_body', 'Test Body')
      ->set('events', ['create'])
      ->save();

    $this->createUser();
    $this->assertMail('id', 'user_register_notify_user_register_notify_create');
    $this->assertMailString('to', 'mail@recipient.com', 1);
    $this->assertMailString('to', 'anotherMail@recipient.com', 1);
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoMultipleRolesMailAddressCreated() {
    $this->createRole([], 'mail_recipient', 'mail_recipient');
    $this->createRole([], 'another_mail_recipient', 'another_mail_recipient');
    // Add an mail recipient account:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'mail@recipient.com',
      'edit-name' => 'User',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');
    // Add another mail recipient account:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'anotherMail@recipient.com',
      'edit-name' => 'User2',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-another-mail-recipient' => TRUE,
    ], 'Create new account');

    $this->moduleSettings
      ->set('type', 'role')
      ->set('roles', ['mail_recipient', 'another_mail_recipient'])
      ->set('created_subject', 'Test Subject')
      ->set('created_body', 'Test Body')
      ->set('events', ['create'])
      ->save();

    $this->createUser();
    $this->assertMail('id', 'user_register_notify_user_register_notify_create');
    $this->assertMailString('to', 'mail@recipient.com', 1);
    $this->assertMailString('to', 'anotherMail@recipient.com', 1);
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoRoleMailAddressCreatedIncludeRole() {
    $this->createRole([], 'mail_recipient', 'mail_recipient');
    $this->createRole([], 'mentioned', 'mentioned');
    $this->createRole([], 'not_mentioned', 'not_mentioned');
    // Add an mail recipient account:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'mail@recipient.com',
      'edit-name' => 'User',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');

    $this->moduleSettings
      ->set('type', 'role')
      ->set('roles', ['mail_recipient'])
      ->set('created_subject', 'Test Subject')
      ->set('created_body', 'Test Body')
      ->set('created_roles_mode', 'include')
      ->set('created_roles', ['mentioned'])
      ->set('events', ['create'])
      ->save();

    // Add a user, which should be mentioned, when creating a mail:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'notMentioned@recipient.com',
      'edit-name' => 'User2',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mentioned' => TRUE,
    ], 'Create new account');

    $this->assertMail('id', 'user_register_notify_user_register_notify_create');
    $this->assertMail('to', 'mail@recipient.com');
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);

    // Add a user, which shouldn't be mentioned, when creating a mail:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'notMentioned@recipient.com',
      'edit-name' => 'User2',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mentioned' => TRUE,
    ], 'Create new account');
    // This user shouldn't create a new mail, so let's count all mails
    // created during this test, which should be 1:
    $mailsCount = count($this->getMails());
    $this->assertSame(1, $mailsCount);
  }

  /**
   * Test sending mails.
   */
  public function testSendMailtoRoleMailAddressCreatedExcludeRole() {
    $this->createRole([], 'mail_recipient', 'mail_recipient');
    $this->createRole([], 'mentioned', 'mentioned');
    $this->createRole([], 'not_mentioned', 'not_mentioned');
    // Add an mail recipient account:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'mail@recipient.com',
      'edit-name' => 'User',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mail-recipient' => TRUE,
    ], 'Create new account');

    $this->moduleSettings
      ->set('type', 'role')
      ->set('roles', ['mail_recipient'])
      ->set('created_subject', 'Test Subject')
      ->set('created_body', 'Test Body')
      ->set('created_roles_mode', 'exclude')
      ->set('created_roles', ['not_mentioned'])
      ->set('events', ['create'])
      ->save();

    // Add a user, which should be mentioned, when creating a mail:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'notMentioned@recipient.com',
      'edit-name' => 'User2',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mentioned' => TRUE,
    ], 'Create new account');

    $this->assertMail('id', 'user_register_notify_user_register_notify_create');
    $this->assertMail('to', 'mail@recipient.com');
    $this->assertMail('subject', 'Test Subject');
    // Body can not be normally tested, since a \n gets added:
    $this->assertMailString('body', 'Test Body', 1);

    // Add a user, which shouldn't be mentioned, when creating a mail:
    $this->drupalGet('/admin/people/create');
    $this->submitForm([
      'edit-mail' => 'notMentioned@recipient.com',
      'edit-name' => 'User2',
      'edit-pass-pass1' => 'pass',
      'edit-pass-pass2' => 'pass',
      'edit-status-1' => TRUE,
      'edit-roles-authenticated' => TRUE,
      'edit-roles-mentioned' => TRUE,
    ], 'Create new account');
    // This user shouldn't create a new mail, so let's count all mails
    // created during this test, which should be 1:
    $mailsCount = count($this->getMails());
    $this->assertSame(1, $mailsCount);
  }

}
